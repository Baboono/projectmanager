package com.productManager.rest.endpoint;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.util.OrderCalculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UtilTest {

    @Test
    public void testCalculateTotalOrderAmount() throws Exception {

        Map<ProductImpl, Long> productMap = new HashMap<>();
        ProductImpl product1 = new ProductImpl("product1",BigDecimal.valueOf(0.25));
        ProductImpl product2 = new ProductImpl("product2",BigDecimal.ONE);
        ProductImpl product3 = new ProductImpl("product3",BigDecimal.ZERO);
        ProductImpl product4 = new ProductImpl("product4",BigDecimal.TEN);
        productMap.put(product1,9L);
        productMap.put(product2,2L);
        productMap.put(product3,62352262L);
        productMap.put(product4,1L);

        BigDecimal orderValue = OrderCalculator.calculateOrderValue(productMap);
        org.junit.Assert.assertEquals(orderValue,new BigDecimal(14.25));

    }



}
