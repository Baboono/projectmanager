package com.productManager.rest.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.interfaces.Product;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Abstract Test with common test methods.
 *
 * @author Radoslaw Baltrukiewicz
 *
 */
public abstract class BaseEndpointTest {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected static final MediaType JSON_MEDIA_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);


    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    protected void setup() throws Exception {

        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    /**
     * For returning JSON representation of an object
     * @param o instance
     * @return String
     * @throws IOException
     */
    protected String json(Object o) throws IOException {

        return objectMapper.writeValueAsString(o);
    }
    /**
     * For returning serialized  loan objects from json instances
     * @param  json string
     * @return Product class object
     * @throws IOException
     * @throws JSONException
     * @throws ParseException
     */
    protected Product convertJSONStringToObject(String json) throws IOException, JSONException, ParseException {
        JSONObject jsonObject = new JSONObject(json);
        BigDecimal amount = new BigDecimal(jsonObject.getString("amount"));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(jsonObject.getString("dueDate").substring(0,19), dateFormatter);
        Instant instant = dateTime.toInstant(OffsetDateTime.now().getOffset());
        Date date = Date.from(instant);
        BigDecimal price = new BigDecimal(jsonObject.getString("price"));
        Product product = new ProductImpl(jsonObject.getString("name"),price);
        product.setDateCreated(date);
        return product;
    }

}
