package com.productManager.rest.endpoint;

import com.productManager.rest.config.ApplicationProperties;
import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.interfaces.Product;
import com.productManager.rest.endpoint.products.ProductEndpoint;
import com.productManager.rest.service.products.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Main test class
 *
 * @author Radoslaw Baltrukiewicz
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class ProductImplEndpointTest extends BaseEndpointTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ProductServiceImpl productServiceImpl;

    @Autowired
    private ApplicationProperties applicationProperties;

    private Product testProductImpl;

    @Before
    public void setup() throws Exception {
        super.setup();

        // create test Products
        productServiceImpl.save(createProduct("TEST PRODUCT 1", new BigDecimal(11.5)));
        productServiceImpl.save(createProduct("TEST PRODUCT 2", new BigDecimal(30)));
        productServiceImpl.save(createProduct("TEST PRODUCT 3", new BigDecimal(20)));


        List<Product> products = productServiceImpl.findAll();
        assertNotNull(products);
        assertEquals(3L, products.size());
        testProductImpl = products.get(0);
        assertNotNull(testProductImpl);
        entityManager.refresh(testProductImpl);
    }

    /**
     * Test create Product success.
     */
    @Test
    public void createProductReturnedSuccess() throws Exception {

        testProductImpl = new ProductImpl("TEST PRODUCT", BigDecimal.TEN);
        mockMvc.perform(post("/v1/product").contentType(JSON_MEDIA_TYPE).content(json(testProductImpl))).andDo(print())
                .andExpect(status().isOk())
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Product returnedProduct = convertJSONStringToObject(json);
                    if (!returnedProduct.getPrice().equals(new BigDecimal(1)))
                        fail("returned amount does not match" + returnedProduct.toString());
                    if (!returnedProduct.getName().equals(1L))
                        fail("returned term does not match" + returnedProduct.toString());
                    LocalDateTime createdProductDueTime =
                            LocalDateTime.ofInstant(returnedProduct.getDateCreated().toInstant(), ZoneId.systemDefault());
                    if (!createdProductDueTime.isBefore(LocalDateTime.now()))
                        fail("Created date cannot be before now" + createdProductDueTime);
                });

    }

    /**
     * Test update Product success.
     */
    @Test
    public void createProductUpdatedSuccess() throws Exception {
        Long id = productServiceImpl.save(createProduct("TEST PRODUCT", new BigDecimal(11.5)));
        mockMvc.perform(put("/v1/product/update").contentType(JSON_MEDIA_TYPE).header("ProductId",
                testProductImpl.getSKU())).andDo(print())
                .andExpect(status().isOk());
        mockMvc.perform(get("/v1/product/{id}", testProductImpl.getSKU())).andDo(print()).andExpect(status().isOk())
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    Product returnedProductImpl = convertJSONStringToObject(json);
                    LocalDateTime createdProductDueTime =
                            LocalDateTime.ofInstant(returnedProductImpl.getDateCreated().toInstant(), ZoneId.systemDefault());
                    if (!createdProductDueTime.isBefore(LocalDateTime.now()))
                        fail("Created date cannot be before now" + createdProductDueTime);
                });


    }

    /**
     * Test application is between current server timeframe and max amount is asked
     */
    @Test
    public void createProductMaxAmountAndServerTime() throws Exception {
        LocalTime localTime = LocalTime.now();
        applicationProperties.setMaxPageSize("0");
        applicationProperties.setMinProductPrice(BigDecimal.ZERO.toString());
        testProductImpl = new ProductImpl("TEST PRODUCT", BigDecimal.TEN);
        mockMvc.perform(post("/v1/product").contentType(JSON_MEDIA_TYPE).content(json(testProductImpl))).andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Test application is not within amount range
     */

    @Test
    public void createProductNotWithinAmount() throws Exception {
        testProductImpl = new ProductImpl("TEST PRODUCT", BigDecimal.TEN);
        mockMvc.perform(post("/v1/product").contentType(JSON_MEDIA_TYPE).content(json(testProductImpl))).andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Test application is not within term range
     */
    @Test
    public void createProductNotWithinTerm() throws Exception {

        testProductImpl = new ProductImpl();
        mockMvc.perform(post("/v1/product").contentType(JSON_MEDIA_TYPE).content(json(testProductImpl))).andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Test not supported method
     */
    @Test
    public void handleHttpRequestMethodNotSupportedException() throws Exception {

        String content = json(testProductImpl);

        mockMvc.perform(
                delete("/v1/product") //not supported method
                        .header(ProductEndpoint.HEADER_PRODUCT_ID, UUID.randomUUID())
                        .accept(JSON_MEDIA_TYPE)
                        .content(content)
                        .contentType(JSON_MEDIA_TYPE))
                .andDo(print())
                .andExpect(status().isMethodNotAllowed())
                .andExpect(content().string(""))
        ;
    }

    private Product createProduct(String name, BigDecimal price) {
        Product product = new ProductImpl(name, price);
        product.setDateCreated(new Date());
        return product;
    }

}
