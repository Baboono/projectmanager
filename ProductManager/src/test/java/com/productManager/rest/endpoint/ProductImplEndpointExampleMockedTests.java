package com.productManager.rest.endpoint;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.interfaces.Product;
import com.productManager.rest.service.products.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Main endpoint mocked tests
 *
 * @author Radoslaw Baltrukiewicz
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class ProductImplEndpointExampleMockedTests extends BaseEndpointTest {

  @Autowired ProductService productService;

  private Product testProductImpl;

  @Before
  public void setup() throws Exception {
    super.setup();
    testProductImpl = new ProductImpl("Test product", BigDecimal.ONE);
  }

  @Test
  public void createProduct() throws Exception {
    mockMvc
        .perform(
            post("/v1/product")
                .contentType(JSON_MEDIA_TYPE)
                .param("name", "Test product")
                .param("price", String.valueOf(BigDecimal.ONE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(JSON_MEDIA_TYPE));
  }

  @Test
  public void updateProduct() throws Exception {
    productService.save(new ProductImpl("Test product", BigDecimal.ONE));
    mockMvc
        .perform(
            put("/v1/product/update")
                .contentType(JSON_MEDIA_TYPE)
                .param("SKU", "1")
                .content(
                    "{\n"
                        + "  \"dateCreated\": \"2021-03-23T00:05:33.305Z\",\n"
                        + "  \"name\": \"new Name\",\n"
                        + "  \"price\": 100\n"
                        + "}"))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void softDeleteProductFail() throws Exception {
    productService.save(new ProductImpl("Test product", BigDecimal.ONE));
    mockMvc
        .perform(delete("/v1/product/softDelete")
            .contentType(JSON_MEDIA_TYPE)
            .param("SKU", "1"))
        .andDo(print())
        .andExpect(status().isNotModified());
  }
}
