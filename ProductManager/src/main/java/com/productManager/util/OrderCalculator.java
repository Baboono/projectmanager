package com.productManager.util;

import com.productManager.rest.domain.ProductImpl;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

public class OrderCalculator {

  public static BigDecimal calculateOrderValue(Map<ProductImpl, Long> productMap) {
    Set<ProductImpl> products = productMap.keySet();
    BigDecimal sum = products
            .parallelStream()
            .map(product -> product.getPrice().multiply(new BigDecimal(productMap.get(product))))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    return sum;
  }
}
