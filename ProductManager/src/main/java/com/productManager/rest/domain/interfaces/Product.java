package com.productManager.rest.domain.interfaces;

import com.productManager.rest.domain.OrderImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


public interface Product {

    void setName(String name);

    BigDecimal getPrice();

    Long getSKU();

    void setSKU(Long SKU);

    String getName();

    void setPrice(BigDecimal price);

    Date getDateCreated();

    void setDateCreated(Date date);

    void setOrders(Set<OrderImpl> orders);

    Set<OrderImpl> getOrders();

    void setDeleted();

}
