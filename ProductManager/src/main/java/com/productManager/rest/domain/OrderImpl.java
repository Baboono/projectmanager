package com.productManager.rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.productManager.rest.domain.interfaces.Order;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * Order entity class
 *
 * @author Radoslaw Baltrukiewicz
 *
 */

@Entity(name = "Order")
@Table(name = "orders")
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ToString
public class OrderImpl implements Order {

    @Id
    @JsonProperty(access = Access.READ_ONLY)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;
    @ManyToMany(mappedBy = "orders",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ProductImpl> products;

    @NotNull
    private String email;

    @NotNull
    @Column(name = "TOTAL_VALUE",nullable = false)
    private BigDecimal totalValue;

    @NotNull
    @JsonProperty(access = Access.READ_ONLY)
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_CREATED",nullable = false)
    private Date dateCreated;

    public OrderImpl(String email){
        setDateCreated(Date.from(java.time.ZonedDateTime.now().toInstant()));
        setEmail(email);
        products = new HashSet<>();
    }
}
