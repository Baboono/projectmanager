package com.productManager.rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.productManager.rest.domain.interfaces.OrderCart;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
@Data
@NoArgsConstructor
public class OrderCartVO implements OrderCart {

    private Map<Long,Long> orderMap = new HashMap<>();
    @JsonIgnore
    private BigDecimal totalOrderValue;
    private String email;

    public OrderCartVO(Map<Long,Long> orderMap){
        this.orderMap=orderMap;
    }
}
