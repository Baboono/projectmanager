package com.productManager.rest.domain.interfaces;

import com.productManager.rest.domain.ProductImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public interface Order {

    Long getId();

    void setId(Long id);

    Set<ProductImpl> getProducts();

    void setProducts(Set<ProductImpl> products);

    String getEmail();

    void setEmail(String email);

    BigDecimal getTotalValue();

    void setTotalValue(BigDecimal totalValue);

    Date getDateCreated();

    void setDateCreated(Date dateCreated);
}
