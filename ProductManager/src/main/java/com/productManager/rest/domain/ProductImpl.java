package com.productManager.rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.productManager.rest.domain.interfaces.Product;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Product entity class
 *
 * @author Radoslaw Baltrukiewicz
 */
@Entity(name = "Product")
@Table(name = "products")
@Where(clause = "DELETED = 0")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@NoArgsConstructor
@ToString
public class ProductImpl implements Product {

    @Id
    @JsonProperty(access = Access.READ_ONLY)
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long SKU;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(name = "DATE_CREATED",nullable = false)
    private Date dateCreated;

    @Column(name = "DELETED")
    private Integer deleted = 0;

    @Override
    public void setDeleted() {
        this.deleted = 1;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    @JsonIgnore
    private Set<OrderImpl> orders;

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(Product.class)) return false;
        Product product = (Product) obj;
        return getSKU().equals(product.getSKU());
    }

    public ProductImpl(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

}