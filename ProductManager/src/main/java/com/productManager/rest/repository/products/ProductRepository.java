package com.productManager.rest.repository.products;

import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Product;

import java.util.List;

/**
 * Default spring repository
 *
 * @author Radoslaw Baltrukiewicz
 */
public interface ProductRepository {

    Product saveAndFlush(Product product);

    Product getOne(Long id);

    List<Product> findAll();

    boolean softDelete(Product product) throws ProductException;
}
