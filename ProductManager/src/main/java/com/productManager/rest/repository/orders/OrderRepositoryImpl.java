package com.productManager.rest.repository.orders;

import com.productManager.rest.domain.OrderImpl;
import com.productManager.rest.domain.interfaces.Order;
import com.productManager.rest.domain.interfaces.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

  private final EntityManager em;

  @Override
  public void save(Order order) {
    try {
      for (Product p : order.getProducts()) {
        p.getOrders().add((OrderImpl) order);
        em.persist(p);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Override
  public Order getOne(Long id) {
    Order order = em.find(OrderImpl.class, id);
    return order;
  }

  @Override
  public List<Order> findAllBetweenDates(Date dateFrom, Date dateTo) {
    List<Order> orders = null;
    try {
      orders = em.createQuery(
                  "SELECT p FROM Order p WHERE p.dateCreated BETWEEN :startDate " + "AND :endDate")
              .setParameter("startDate", dateFrom, TemporalType.DATE)
              .setParameter("endDate", dateTo, TemporalType.DATE)
              .getResultList();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return orders;
  }

  public OrderRepositoryImpl(EntityManager em) {
    this.em = em;
  }
}
