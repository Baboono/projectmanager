package com.productManager.rest.repository.products;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

  private final EntityManager em;

  @Override
  public Product saveAndFlush(Product product) {

    if (product.getSKU() == null) {
      product.setDateCreated(Date.from(java.time.ZonedDateTime.now().toInstant()));
      em.persist(product);
    } else {
      product = em.merge(product);
    }
    em.flush();
    return product;
  }

  @Override
  public Product getOne(Long id) {
    Product product = em.find(ProductImpl.class, id);
    return product;
  }

  @Override
  public List<Product> findAll() {
    Query query = em.createQuery("SELECT products FROM Product products");
    return (List<Product>) query.getResultList();
  }

  @Override
  public boolean softDelete(Product product) throws ProductException {
    try {
      product.setDeleted();
      em.flush();
      em.clear();
      return true;
    } catch (Exception e) {
      throw new ProductException(e.getLocalizedMessage());
    }
  }

  public ProductRepositoryImpl(EntityManager em) {
    this.em = em;
  }
}
