package com.productManager.rest.repository.orders;

import com.productManager.rest.domain.interfaces.Order;

import java.util.Date;
import java.util.List;

/**
 * Order repository
 *
 * @author Radoslaw Baltrukiewicz
 *
 */
public interface OrderRepository {

    void save(Order order);

    Order getOne(Long id);

    List<Order> findAllBetweenDates(Date dateFrom, Date dateTo);
}