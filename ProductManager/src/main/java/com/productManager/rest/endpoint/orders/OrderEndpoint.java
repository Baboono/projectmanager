package com.productManager.rest.endpoint.orders;

import com.productManager.rest.domain.OrderCartVO;
import com.productManager.rest.domain.exception.orders.OrderException;
import com.productManager.rest.domain.interfaces.Order;
import com.productManager.rest.endpoint.BaseEndpoint;
import com.productManager.rest.service.orders.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Controller Class responsible for recieving order requests as JSON, parsing them and pushing
 * further to business logic in Service(s)
 *
 * @author Radoslaw Baltrukiewicz
 */
@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
@Validated
public class OrderEndpoint extends BaseEndpoint {

    private static final Logger log = LoggerFactory.getLogger(OrderEndpoint.class);
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final String HEADER_order_ID = "orderId";

    @Autowired OrderService orderService;

    @RequestMapping(path = "/v1/orders", method = RequestMethod.GET)
    @ApiOperation(value = "Get all orders", notes = "Returns first N orders between dates. Dates must be in ISO date" +
            "format: YYYY-MM-DD Example : 2020-03-22")
    public ResponseEntity<Object> getAll(
            @NotNull @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) @RequestParam(required = true) Date dateFrom,
            @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) @RequestParam(required = true) @NotNull Date dateTo) {
        List<Order> orders = new ArrayList<>();
        ResponseEntity responseEntity = null;
        try {
            orders=orderService.findAllBetweenDates(dateFrom,dateTo);
        } catch (Exception e) {
            log.warn(messageSource.getMessage("orders.notRetrieved", null, Locale.getDefault()));
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(orders);
        }
        log.info(messageSource.getMessage("orders.getAllSuccess", null, Locale.getDefault()));
        return (orders.isEmpty() ? ResponseEntity.status(HttpStatus.NOT_FOUND) : ResponseEntity.ok())
                .body(orders);
    }

    @RequestMapping(path = "/v1/order",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a order",notes = "Creates a order. Returns unique order`s  id",response = Long.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Succesfully created order"),
                    @ApiResponse(code = 400, message = "Validation criteria is not met") })
    public ResponseEntity<Object> createOrder(
            @ApiParam(value = "Cart with map of products and quantities and email ", required = true)
            @RequestBody
                    OrderCartVO cartVO) {
        try {
            orderService.save(cartVO);
            log.info(messageSource.getMessage("order.added", null, Locale.getDefault()));
            return ResponseEntity.ok().build();
        } catch (OrderException e) {
            log.warn(messageSource.getMessage("order.addedFailed", null, Locale.getDefault()));
            return ResponseEntity.badRequest().build();
        }
    }
}
