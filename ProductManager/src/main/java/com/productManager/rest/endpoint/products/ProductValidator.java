package com.productManager.rest.endpoint.products;

import com.productManager.rest.config.ApplicationProperties;
import com.productManager.rest.domain.interfaces.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

/**
 * Product validation
 *
 * @author Radoslaw Baltrukiewicz
 */
@Component
public class ProductValidator implements Validator {

  public static final String PRODUCT_REJECTED = "Product rejected";
  @Autowired private ApplicationProperties applicationProperties;

  @Override
  public boolean supports(Class<?> clazz) {
    return Product.class.isAssignableFrom(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {

    Product product = null;
    try {
      product = (Product) target;
    } catch (ClassCastException e) {
      errors.rejectValue("Target ", "Must be type of Product" + PRODUCT_REJECTED);
    }
    BigDecimal amount = product.getPrice();
    String name = product.getName();
    if (name == null) {
      throw new NullPointerException("Product name cannot be null" + PRODUCT_REJECTED);
    }
    if (name.isEmpty() || name.startsWith(" ")) {
      errors.rejectValue("term", "", "Product term cannot be negative." + PRODUCT_REJECTED);
    }
    if (amount == null) {
      throw new NullPointerException("Product amount cannot be null" + PRODUCT_REJECTED);
    }
    if (amount.compareTo(
            BigDecimal.valueOf(Double.valueOf(applicationProperties.getMinProductPrice())))
        == -1) {
      errors.rejectValue("amount", "", "Product amount cannot be negative." + PRODUCT_REJECTED);
    }
    if (product.getSKU() == null || product.getSKU() < 1) {
      errors.rejectValue("SKU", "", "Product ID(SKU) must be LONG above 0" + PRODUCT_REJECTED);
    }
  }
}
