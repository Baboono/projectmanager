package com.productManager.rest.endpoint.products;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Product;
import com.productManager.rest.endpoint.BaseEndpoint;
import com.productManager.rest.service.products.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Class responsible for recieving product requests as JSON, parsing them and pushing further to
 * business logic in Service(s)
 *
 * @author Radoslaw Baltrukiewicz
 */
@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
@Validated
public class ProductEndpoint extends BaseEndpoint {

    private static final Logger log = LoggerFactory.getLogger(ProductEndpoint.class);
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final String HEADER_PRODUCT_ID = "productId";

    @Autowired ProductService productService;
    @Autowired ProductValidator productValidator;

    @RequestMapping(path = "/v1/products", method = RequestMethod.GET)
    @ApiOperation(value = "Get all products",notes ="Returns first N products specified by the size parameter with "
                    + "page offset specified by page parameter.")
    public ResponseEntity<Object> getAll(
            @ApiParam("The size of the page to be returned") @RequestParam(required = false) Integer size) {
        ResponseEntity responseEntity = null;
        if (size == null) {
            size = DEFAULT_PAGE_SIZE;
        }
        try {
            List<Product> products = productService.findAll();
            log.info(messageSource.getMessage("products.getAllSuccess", null, Locale.getDefault()));
            return (products.isEmpty() ? ResponseEntity.status(HttpStatus.NOT_FOUND): ResponseEntity.ok())
                    .body(products.stream().limit(size).collect(Collectors.toList()));
        } catch (Exception e) {
            log.warn(messageSource.getMessage("products.notRetrieved", null, Locale.getDefault()));
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(messageSource.getMessage("products.notRetrieved", null, Locale.getDefault()));
        }
    }

    @RequestMapping(path = "/v1/product",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Creates a product",notes = "Creates a product. Returns unique product`s  SKU",
            response = Long.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Succesfully created product"),
            @ApiResponse(code = 400, message = "Validation criteria is not met")})
    public ResponseEntity<Object> createProduct(@NotNull @NotEmpty @RequestParam(required = true) String name,
                                                @Min(0) @RequestParam(required = true) BigDecimal price) {
        try {
            Long productSKU = productService.save(new ProductImpl(name, price));
            log.info(messageSource.getMessage("product.added", null, Locale.getDefault())
                    + " Id:"+ productSKU);
            return ResponseEntity.ok().body(productSKU);
        } catch (ProductException e) {
            log.warn(messageSource.getMessage("product.applicationFailed", null, Locale.getDefault()));
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(path = "/v1/product/update", method = RequestMethod.PUT)
    @ApiOperation(value = "Update existing product",notes = "Product must exist. Due date of " + "product gets updated",
            response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Succesfully updated product"),
            @ApiResponse(code = 400,message ="Wrong type of parameters are supplied"),
            @ApiResponse(code = 404, message = "product with this id is not found"),
            @ApiResponse(code = 304, message = "product was found but could not updated")})
    public ResponseEntity<Object> update(@Valid @ApiParam(value = "the product to update", required = true) @RequestBody
                    ProductImpl product,@Min(1) @RequestParam(required = true) long SKU) {
        try {
            productService.updateProduct(product,SKU);
        } catch (ProductException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/v1/product/softDelete", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes product if exists",
            notes = "Performs soft delete operation on product. Returns OK if succesfully deleted",
            response = Boolean.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful deletion product"),
            @ApiResponse(code = 400,message ="Wrong type of parameters are supplied"),
            @ApiResponse(code = 404, message = "product with this id is not found"),
            @ApiResponse(code = 304, message = "product was found but could not deleted")
    })
    public ResponseEntity<Object> softDelete(@ApiParam(value ="Unique product ID")
            @Min(1) @RequestParam(required = true) long SKU) {
        try {
            productService.softDeleteProduct(SKU);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        }

    }

    @InitBinder("product")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(productValidator);
    }
}
