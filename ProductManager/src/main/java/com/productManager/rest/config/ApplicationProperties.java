package com.productManager.rest.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


/**
 * Configuration. Taken from application.properties file
 *
 * @author Radoslaw Baltrukiewicz
 */
@Component
@PropertySource("classpath:application.properties")
@Getter
@Setter
public class ApplicationProperties {

    @Value("${productManagerDemo.config.product.minProductPrice}")
    private String minProductPrice;

    @Value("${productManagerDemo.config.product.maxPageSize}")
    private String maxPageSize;



}
