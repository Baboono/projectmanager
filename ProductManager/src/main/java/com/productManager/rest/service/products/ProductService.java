package com.productManager.rest.service.products;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Product;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductService {
  @Transactional(readOnly = true)
  List<Product> findAll() throws ProductException;

  @Transactional
  Long save(Product product) throws ProductException;

  @Transactional(readOnly = true)
  Product findOne(Long id) throws ProductException;

  void updateProduct(ProductImpl product, long sku) throws ProductException;

  @Transactional
  void softDeleteProduct(long sku) throws ProductException;
}
