package com.productManager.rest.service.products;

import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Product;
import com.productManager.rest.repository.products.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Service involved for all product entity based operations
 *
 * @author Radoslaw Baltrukiewicz
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

  @Autowired private ProductRepository repository;

  @Autowired protected MessageSource messageSource;

  private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

  private final long noId = -1L;

  @Override
  public List<Product> findAll() throws ProductException {
    List<Product> products = new ArrayList<>();
    try {
      products.addAll(repository.findAll());
    } catch (Exception e) {
      log.error(e.getMessage());
      generateProductException(
          "Could not extract list of products from database. Exception: " + e.getMessage(), noId);
    }
    return products;
  }

  @Override
  public Product findOne(Long id) throws ProductException {
    Product product = new ProductImpl();
    try {
      product = repository.getOne(id);
    } catch (Exception e) {
      log.error(e.getMessage());
      generateProductException(
          "Could not get product with id" + id + " from database. Exception: " + e.getMessage(),id);
    }
    return product;
  }

  @Override
  public Long save(Product product) throws ProductException {
    Long SKU = -1L;
    try {
      SKU = repository.saveAndFlush(product).getSKU();
    } catch (Exception e) {
      log.error(e.getMessage());
      generateProductException(
          "Could not create product. Rolling back transaction.  Exception is: " + e.getMessage(),SKU);
    }
    return SKU;
  }

  @Override
  public void updateProduct(ProductImpl product, long SKU) throws ProductException {
    try {
      if (findOne(SKU) == null) {
        generateProductException(
            messageSource.getMessage("product.notFound", null, Locale.getDefault()) + " ID: " + SKU,noId);
      }
      product.setSKU(SKU);
      log.info(messageSource.getMessage("product.found", null, Locale.getDefault()) + " Id:" + SKU);
      save(product);
      log.info(messageSource.getMessage("product.updated", null, Locale.getDefault()) + " Id:" + SKU);
    } catch (Exception e) {
      log.error(e.getMessage());
      generateProductException("product.notUpdated", SKU);
    }
  }

  @Override
  public void softDeleteProduct(long SKU) throws ProductException {
    Product product = findOne(SKU);
    if (product == null) {
      generateProductException(
          messageSource.getMessage("product.notFound", null, Locale.getDefault()) + " ID: " + SKU,noId);
    } else {
      try {
        repository.softDelete(product);
      } catch (ProductException e) {
        log.error(e.getMessage());
        generateProductException(
            messageSource.getMessage("product.notDeleted", null, Locale.getDefault())
                + " ID: "+ SKU,noId);
      }
    }
  }

  private String generateProductException(String message, long SKU) throws ProductException {
    log.error(message);
    String exceptionMessage =
        messageSource.getMessage(message, null, Locale.getDefault()) + " Id:" + SKU;
    log.error(exceptionMessage);
    throw new ProductException(exceptionMessage);
  }
}
