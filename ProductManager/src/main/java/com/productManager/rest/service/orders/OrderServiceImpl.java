package com.productManager.rest.service.orders;

import com.productManager.rest.domain.OrderCartVO;
import com.productManager.rest.domain.OrderImpl;
import com.productManager.rest.domain.ProductImpl;
import com.productManager.rest.domain.exception.orders.OrderException;
import com.productManager.rest.domain.exception.products.ProductException;
import com.productManager.rest.domain.interfaces.Order;
import com.productManager.rest.repository.orders.OrderRepository;
import com.productManager.rest.service.products.ProductService;
import com.productManager.util.OrderCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

  @Autowired OrderRepository orderRepository;
  @Autowired ProductService productService;

  @Override
  public List<Order> findAllBetweenDates(Date dateFrom, Date dateTo) throws OrderException {
    try {
      return orderRepository.findAllBetweenDates(dateFrom, dateTo);
    } catch (Exception e) {
      throw new OrderException(
          "Could not get orders list from database. Exception: " + e.getMessage());
    }
  }

  @Override
  public Order findOne(Long id) throws OrderException {
    try {
      return orderRepository.getOne(id);
    } catch (Exception e) {
      throw new OrderException(
          "Could not get order with id" + id + " from database. Exception: " + e.getMessage());
    }
  }

  @Override
  public void save(OrderCartVO orderCartVO) throws OrderException {
    try {
      orderRepository.save(getOrderFromCart(orderCartVO));
    } catch (Exception e) {
      throw new OrderException(
          "Could not create order. Rolling back transaction.  Exception is: " + e.getMessage());
    }
  }

  private Order getOrderFromCart(OrderCartVO orderCartVO) {
    Map<ProductImpl, Long> productMap = new HashMap<>();
    Map<Long, Long> orderMap = orderCartVO.getOrderMap();
    Order order = new OrderImpl(orderCartVO.getEmail());
    orderMap.keySet().stream().forEach(
            id -> {
              try {
                Long quantity = orderMap.get(id);
                ProductImpl product = (ProductImpl) productService.findOne(id);
                order.getProducts().add(product);
                productMap.put(product, quantity);
              } catch (ProductException e) {
                e.printStackTrace();
              } catch (ClassCastException e) {
                e.printStackTrace();
              }
            });
    order.setDateCreated(Date.from(java.time.ZonedDateTime.now().toInstant()));
    order.setTotalValue(OrderCalculator.calculateOrderValue(productMap));
    return order;
  }
}
