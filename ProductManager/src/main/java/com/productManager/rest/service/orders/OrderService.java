package com.productManager.rest.service.orders;

import com.productManager.rest.domain.OrderCartVO;
import com.productManager.rest.domain.exception.orders.OrderException;
import com.productManager.rest.domain.interfaces.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface OrderService {

  @Transactional(readOnly = true)
  List<Order> findAllBetweenDates(Date dateFrom, Date dateTo) throws OrderException;

  @Transactional(readOnly = true)
  Order findOne(Long id) throws OrderException;

  @Transactional
  void save(OrderCartVO orderCartVO) throws OrderException;
}
