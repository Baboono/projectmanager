create table ORDERS
(
    ID           LONG      not null,
    EMAIL        VARCHAR2  not null,
    TOTAL_VALUE  DOUBLE    not null,
    DATE_CREATED TIMESTAMP not null,
    constraint ORDERS_PK
        primary key (ID),
    constraint ORDERS_PRODUCTS_SKU_FK
        foreign key (ID) references PRODUCTS (SKU)
);

create table PRODUCTS
(
    SKU          LONG auto_increment,
    NAME         VARCHAR2 not null,
    PRICE        DOUBLE   not null,
    DATE_CREATED TIMESTAMP not null,
    DELETED      INT,
    constraint PRODUCTS_PK
        primary key (SKU),
    constraint PRODUCTS_ORDERS_ID_FK
        foreign key (SKU) references ORDERS (ID)
);

create table ORDERS_PRODUCTS
(
    SKU LONG,
    ID  LONG,
    constraint ORDERS_PRODUCTS_PRODUCTS_SKU_FK
        foreign key (SKU) references PRODUCTS (SKU),
    constraint ORDERS_PRODUCTS___FK2
        foreign key (ID) references ORDERS (ID)
);

