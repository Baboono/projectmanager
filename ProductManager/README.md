# Product manager Demo
Standard maven/spring boot configuration.
<mvn clean install> to build the project.
<mvn spring-boot:run> to run the project.

If run locally server available on http://localhost:8090/ + ENDPOINT

All external property configuration placed in application.properties file.

For simplification project is using in memory h2 database.

For more information about productImpl endpoints run get  http://localhost:8090/swagger-ui.html 
To start testing, please create a few products using swagger menu by sending post to adress:
| POST   |/v1/product     | add product|
then You can retrive created products using
| GET    |/v1/products    | get N products with an offset|
and further having SKU, delete and update operations can be performed
| PUT    |/v1/product/update     | updates product|
| DELETE |/v1/productImpl/delete | deletes product|

After we are done with the product we can proceed to creating orders


Also for no compiler error I strongly recommend to install projectLombook
into Your IDE :
IntelliJ https://plugins.jetbrains.com/plugin/6317-lombok
Eclipse https://projectlombok.org/setup/eclipse

There is also messaging and logger configuration as extras
Because of lack of time, some things were left unfinished

Architecture patterns: TDD was used to perform mocked CRUD operations on REST and further go deeper into logic and persistence.
DDD was used to split product domains and create value objects.
Standard KISS and SOLID rules apply for seggregation of interfaces, method isolation, dependance over abstraction, rather than details, whole program was to be kept as simple as possible. 

Happy reviewing!

### Endpoints

| Method | Url | Decription |
| GET    |/swagger-ui.html| swagger html |      MAIN ENDPOINT
| ------ | --- | ---------- |
SERVER DATA
| GET    |/actuator/info  | info / heartbeat - provided by boot |
| GET    |/actuator/health| application health - provided by boot |
| GET    |/v2/api-docs    | swagger json |

PRODUCTS
| GET    |/v1/products    | get N products with an offset|
| POST   |/v1/product     | add product|
| PUT    |/v1/product/update     | updates product|
| DELETE |/v1/product/softDelete | deletes product|

ORDERS
| POST    |/v1/order    | adds order|
We create cars JSON with email and orderMap
OrderMap contains list of product SKU`s and quantities
{
  "email": "string1",
  "orderMap": {"1":5,"3":1,"2":10}
}
| GET |/v1/orders | gets orders in date range|
Dates must be in ISO dateformat: YYYY-MM-DD Example : 2020-03-22